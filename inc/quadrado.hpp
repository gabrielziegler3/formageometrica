#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"

class Quadrado : public FormaGeometrica {
    public:
        Quadrado();
        Quadrado(float base);
        //~Quadrado();
        
        //void calculaArea();
	//void calculaPerimetro();
		
};

#endif
